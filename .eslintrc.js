module.exports = {
  extends: [
    "standard",
    "plugin:vue/recommended"
  ],
  rules: {
    quotes: [
      "error",
      "double"
    ]
  }
}
