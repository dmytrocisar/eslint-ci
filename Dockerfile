FROM node:12
WORKDIR /app
COPY Makefile /app
COPY package.json /app
COPY .eslintrc.js /app
RUN npm install
