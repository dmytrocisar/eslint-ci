#!/usr/bin/make

IMAGE ?= 'dmytrocisar/eslint'

build:
	docker build -f Dockerfile -t $(IMAGE):latest .

push:
	docker push $(IMAGE)

lint:
	docker run --rm -i -v $(shell pwd)/src:/app/src -w /app/src -it $(IMAGE) npm run lint

fix:
	docker run --rm -i -v $(shell pwd)/src:/app/src -w /app/src -it $(IMAGE) npm run fix

# experiments
echo:
	docker run --rm -i -v $(shell pwd)/src:/app/src -w /app/src -it $(IMAGE) npm run echo


